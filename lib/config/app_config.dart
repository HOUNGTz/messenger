import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:messenger/config/app_controller.dart';
import 'package:messenger/config/app_route.dart';
import 'package:messenger/constants/app_constant.dart';
import 'package:messenger/modules/bottom_bar.dart';

class ChatApp extends StatelessWidget {
  const ChatApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: AppConstant.appName,
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      themeMode: ThemeMode.system,
      initialBinding: AppController(),
      supportedLocales: AppConstant.supportLocalLanguage,
      localizationsDelegates: AppConstant.delegate,
      //locale: _locale,
      fallbackLocale: AppConstant.fallbacklocal,
      home: const MyBottomNavigationBar(),
      onGenerateRoute: AppRoute.generateRoute(context),
      builder: EasyLoading.init(),
    );
  }
}