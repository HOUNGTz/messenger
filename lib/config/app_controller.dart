import 'package:get/instance_manager.dart';
import 'package:messenger/modules/chat/chat_controller.dart';
import 'package:messenger/modules/chat/chat_repository.dart';
import 'package:messenger/modules/people/people_controller.dart';
import 'package:messenger/modules/people/people_repository.dart';
import 'package:messenger/modules/public_chat/public_chat_controller.dart';
import 'package:messenger/modules/public_chat/public_chat_repository.dart';
import 'package:messenger/modules/setting/setting_controller.dart';
import 'package:messenger/modules/setting/setting_repository.dart';

class AppController extends Bindings{

  @override
  void dependencies() {
    Get.lazyPut(() => ChatController(
        repository: ChatRepository()
      )
    );
    Get.lazyPut(() => PeopleController(
        repository: PeopleRepository()
      )
    );
    Get.lazyPut(() => PublicChatController(
        repository: PublicChatRepository()
      )
    );
    Get.lazyPut(() => SettingController(
        repository: SettingRepository()
      )
    );
  }
}