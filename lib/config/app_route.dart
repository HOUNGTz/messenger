import 'package:flutter/material.dart';
import 'package:messenger/config/route_header.dart';
class AppRoute {
  static const String home = '/home';
  static const String login = '/login';
  static const String register = '/register';

  static Route<dynamic>? Function(RouteSettings)? generateRoute(BuildContext context) {
    return (RouteSettings settings) {
      switch (settings.name) {
        case home:
          final args = settings.arguments as String;
          return RouteHeader.chatPageBuilder(context,args, args);
        case login:
          return RouteHeader.loginPageBuilder();
        case register:
          return RouteHeader.registerPageBuilder();
        default:
          return RouteHeader.errorRoute();
      }
    };
  }
}
