import 'package:flutter/material.dart';
import 'package:messenger/modules/authentication/login_screen.dart';
import 'package:messenger/modules/authentication/register_screen.dart';
import 'package:messenger/modules/bottom_bar.dart';

class RouteHeader {
  static Route<dynamic> errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Error'),
        ),
        body: const Center(
          child: Text('Page not found'),
        ),
      );
    });
  }

  static Route<dynamic> chatPageBuilder(BuildContext context, String currentUserEmail, String username) {
    return MaterialPageRoute(
      builder: (context) => const MyBottomNavigationBar(),
    );
  }

  static Route<dynamic> loginPageBuilder(){
    return MaterialPageRoute(
      builder: (_) => const LoginScreen()
    );
  }

  static Route<dynamic> registerPageBuilder(){
    return MaterialPageRoute(builder: (_) => const RegisterScreen());
  }
}