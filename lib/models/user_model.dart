class UserModel {
  String? id;
  String? email;
  String? firstName;
  String? lastName;
  String? userName;
  String? profileUrl;
  int? permission;

  UserModel({
    this.id, 
    this.email, 
    this.firstName, 
    this.lastName,
    this.userName,
    this.profileUrl,
    this.permission,
  });

  factory UserModel.fromMap(map) {
    return UserModel(
      id: map['uid'],
      email: map['email'],
      firstName: map['firstName'],
      lastName: map['lastName'],
      userName: map['username'],
      profileUrl: map['profile'],
      permission: map['permission'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': id,
      'email': email,
      'firstName': firstName,
      'lastName': lastName,
      'username': userName,
      'profile': profileUrl,
      'permission': permission,
      
    };
  }
}