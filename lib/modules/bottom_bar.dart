import 'package:flutter/material.dart';
import 'package:messenger/modules/chat/chat_page.dart';
import 'package:messenger/modules/people/people_page.dart';
import 'package:messenger/modules/public_chat/public_chat_page.dart';
import 'package:messenger/modules/setting/setting_page.dart';

class MyBottomNavigationBar extends StatefulWidget {
  const MyBottomNavigationBar({super.key, this.currentUser, this.username});
  final String? currentUser;
  final String? username;

  @override
  State<MyBottomNavigationBar> createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  late List<Widget> screens;
  int index = 0;
  
  @override
  void initState() {
    super.initState();
    screens = [
      ChatPage(currentUserEmail: widget.currentUser ?? '', username: widget.username ?? '',),
      const PeoplePage(), 
      PublicChatPage(username: widget.username ?? '',),
      const SettingPage(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[index],
      bottomNavigationBar: _buildBottom(context),
    );
  }

  Widget _buildBottom(BuildContext context) {
    return NavigationBarTheme(
      data: NavigationBarThemeData(
        indicatorColor: Colors.blue.shade100,
        labelTextStyle: MaterialStateProperty.all(
          const TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
        ),
      ),
      child: NavigationBar(
        height: 80,
        backgroundColor: const Color(0xFFf1f5fb),
        selectedIndex: index,
        labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
        animationDuration: const Duration(microseconds: 100),
        onDestinationSelected: (index) => setState(() => this.index = index),
        destinations: const [
          NavigationDestination(
            icon: Icon(Icons.chat_outlined),
            selectedIcon: Icon(Icons.chat),
            label: 'Chats',
          ),
          NavigationDestination(
            icon: Icon(Icons.people_outline),
            selectedIcon: Icon(Icons.people),
            label: 'Friends',
          ),
          NavigationDestination(
            icon:Icon(Icons.public_outlined),
            selectedIcon: Icon(Icons.public),
            label: 'Public Chats',
          ),
          NavigationDestination(
            icon:Icon(Icons.settings_outlined),
            selectedIcon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
      ),
    );
  }
}
