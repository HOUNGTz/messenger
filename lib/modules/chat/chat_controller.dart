import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:messenger/modules/authentication/login_screen.dart';
import 'package:messenger/modules/chat/chat_model.dart';
import 'package:messenger/modules/chat/chat_repository.dart';
import 'package:rxdart/rxdart.dart' as rxdart;

class ChatController extends GetxController {
  ChatController({required this.repository});

  final ChatRepository repository;
  var chat = <ChatModel>[].obs;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  List<ChatModel> get messages => chat;

  void addMessage(ChatModel message) {
    chat.add(message);
  }

  Stream<List<QueryDocumentSnapshot>> fetchLastMessage(String email) {
    var currentUser = _auth.currentUser;

    var sentMessagesStream = _firestore
        .collection('messages')
        .where('sender', isEqualTo: currentUser?.email)
        .where('receiver', isEqualTo: email)
        .orderBy('timestamp', descending: true)
        .limit(1)
        .snapshots();

    var receivedMessagesStream = _firestore
        .collection('messages')
        .where('sender', isEqualTo: email)
        .where('receiver', isEqualTo: currentUser?.email)
        .orderBy('timestamp', descending: true)
        .limit(1)
        .snapshots();

    return rxdart.Rx.combineLatest2(
      sentMessagesStream,
      receivedMessagesStream,
      (QuerySnapshot sentMessages, QuerySnapshot receivedMessages) {
        var allMessages = [...sentMessages.docs, ...receivedMessages.docs];
        allMessages.sort((a, b) =>
            (a['timestamp'] as Timestamp).compareTo(b['timestamp'] as Timestamp));
        return allMessages;
      },
    ).handleError((error) {
      return [];
    });
  }

  Future<void> logout(BuildContext context) async {
    await _auth.signOut();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const LoginScreen()),
    );
  }
}
