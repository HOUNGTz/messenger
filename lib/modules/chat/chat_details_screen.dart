import 'package:flutter/material.dart';
import 'package:get/instance_manager.dart';
import 'package:messenger/modules/chat/chat_controller.dart';
import 'package:messenger/modules/chat/chat_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';

class ChatDetailScreen extends StatefulWidget {
  final String email;
  final String profileImage;
  final int permission;
  final String? username;

  const ChatDetailScreen({
    super.key,
    required this.email,
    required this.profileImage,
    required this.permission,
    this.username,
  });

  @override
  State<ChatDetailScreen> createState() => _ChatDetailScreenState();
}

class _ChatDetailScreenState extends State<ChatDetailScreen> {
  final TextEditingController _messageController = TextEditingController();
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  void _sendMessage() async {
  if (_messageController.text.trim().isEmpty) return;

  var currentUser = _auth.currentUser;
  if (currentUser == null) return;

  // Save message locally (optional for smooth UI update)
  var newMessage = ChatModel(
    sender: currentUser.email!,
    receiver: widget.email,
    timestamp: DateTime.now(),
    content: _messageController.text.trim(),
  );

  Get.find<ChatController>().addMessage(newMessage);

  await _firestore.collection('messages').add({
    'sender': newMessage.sender,
    'receiver': newMessage.receiver,
    'timestamp': newMessage.timestamp,
    'content': newMessage.content,
  });

  _messageController.clear();

  _scrollController.animateTo(
    _scrollController.position.maxScrollExtent,
    duration: const Duration(milliseconds: 300),
    curve: Curves.easeOut,
  );
}

  void _editMessage(String messageId, String newContent) async {
    await _firestore.collection('messages').doc(messageId).update({
      'content': newContent,
    });
  }

  void _deleteMessage(String messageId) async {
    await _firestore.collection('messages').doc(messageId).delete();
  }

  Stream<List<QueryDocumentSnapshot>> _fetchMessages() {
    var currentUser = _auth.currentUser;
    var sentMessagesStream = _firestore
        .collection('messages')
        .where('sender', isEqualTo: currentUser?.email)
        .where('receiver', isEqualTo: widget.email)
        .snapshots();

    var receivedMessagesStream = _firestore
        .collection('messages')
        .where('sender', isEqualTo: widget.email)
        .where('receiver', isEqualTo: currentUser?.email)
        .snapshots();

    return Rx.combineLatest2(sentMessagesStream, receivedMessagesStream,
        (QuerySnapshot sentMessages, QuerySnapshot receivedMessages) {
      var allMessages = [...sentMessages.docs, ...receivedMessages.docs];
      allMessages.sort((a, b) => (a['timestamp'] as Timestamp)
          .compareTo(b['timestamp'] as Timestamp));
      return allMessages;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
    );
  }

  PreferredSizeWidget _buildAppBar() {
    return AppBar(
      title: Row(
        children: [
          CircleAvatar(
            backgroundImage: NetworkImage(widget.profileImage),
            radius: 20,
          ),
          const SizedBox(width: 12),
          Text(widget.username ?? ''),
        ],
      ),
      actions: [
        IconButton(
          onPressed: () {},
          icon: const Icon(Icons.call),
        ),
        IconButton(
          onPressed: () {},
          icon: const Icon(Icons.video_call),
        ),
      ],
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        Expanded(
          child: StreamBuilder<List<QueryDocumentSnapshot>>(
            stream: _fetchMessages(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              }

              if (snapshot.hasError) {
                return const Center(child: CircularProgressIndicator());
              }

              var allMessages = snapshot.data ?? [];

              if (allMessages.isEmpty) {
                return const Center(
                    child: Text('No messages yet. Start the conversation!'));
              }

              WidgetsBinding.instance.addPostFrameCallback((_) {
                if (_scrollController.hasClients) {
                  _scrollController.animateTo(
                    _scrollController.position.maxScrollExtent,
                    duration: const Duration(milliseconds: 300),
                    curve: Curves.easeOut,
                  );
                }
              });

              return ListView.builder(
                controller: _scrollController,
                itemCount: allMessages.length,
                itemBuilder: (context, index) {
                  var message = allMessages[index];
                  var isSender =
                      message['sender'] == _auth.currentUser?.email;
                  var messageTime =
                      (message['timestamp'] as Timestamp).toDate();
                  var timeFormat = DateFormat('HH:mm').format(messageTime);

                  return GestureDetector(
                    onLongPress: () => _showMessageOptions(context, message),
                    child: Align(
                      alignment:
                          isSender ? Alignment.centerRight : Alignment.centerLeft,
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 8.0),
                        padding: const EdgeInsets.all(12.0),
                        decoration: BoxDecoration(
                          color: isSender ? Colors.blue : Colors.grey[200],
                          borderRadius: BorderRadius.circular(12.0),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              message['content'] ?? 'No content',
                              style: TextStyle(
                                color: isSender ? Colors.white : Colors.black,
                                fontSize: 16.0,
                              ),
                            ),
                            const SizedBox(height: 4.0),
                            Text(
                              timeFormat,
                              style: TextStyle(
                                color: isSender ? Colors.white70 : Colors.black54,
                                fontSize: 12.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            },
          ),
        ),
        _buildFooter(),
      ],
    );
  }

  void _showMessageOptions(BuildContext context, QueryDocumentSnapshot message) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: const Icon(Icons.edit),
              title: const Text('Edit'),
              onTap: () {
                Navigator.pop(context);
                _showEditDialog(context, message);
              },
            ),
            ListTile(
              leading: const Icon(Icons.delete),
              title: const Text('Delete'),
              onTap: () {
                Navigator.pop(context);
                _deleteMessage(message.id);
              },
            ),
          ],
        );
      },
    );
  }

  void _showEditDialog(BuildContext context, QueryDocumentSnapshot message) {
    TextEditingController _editController = TextEditingController(
        text: message['content']);

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Edit Message'),
          content: TextField(
            controller: _editController,
            decoration: const InputDecoration(hintText: 'Edit your message'),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                _editMessage(message.id, _editController.text.trim());
                Navigator.pop(context);
              },
              child: const Text('Save'),
            ),
          ],
        );
      },
    );
  }

  Widget _buildFooter() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: _messageController,
              decoration: InputDecoration(
                hintText: 'Type a message...',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(24.0),
                ),
                contentPadding: const EdgeInsets.symmetric(horizontal: 16.0),
              ),
              textInputAction: TextInputAction.send,
              keyboardType: TextInputType.text,
              onSubmitted: (_) {
                _sendMessage();
              },
            ),
          ),
          const SizedBox(width: 8.0),
          IconButton(
            icon: const Icon(Icons.send),
            onPressed: _sendMessage,
          ),
        ],
      ),
    );
  }
}