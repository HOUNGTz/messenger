import 'package:cloud_firestore/cloud_firestore.dart';

class ChatModel {
  final String sender;
  final String receiver;
  final DateTime timestamp;
  final String content;

  ChatModel({
    required this.sender,
    required this.receiver,
    required this.timestamp,
    required this.content,
  });

  Map<String, dynamic> toMap() {
    return {
      'sender': sender,
      'receiver': receiver,
      'timestamp': timestamp,
      'content': content,
    };
  }

  factory ChatModel.fromMap(Map<String, dynamic> map) {
    return ChatModel(
      sender: map['sender'],
      receiver: map['receiver'],
      timestamp: (map['timestamp'] as Timestamp).toDate(),
      content: map['content'],
    );
  }
}
