import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:get/instance_manager.dart';
import 'package:messenger/modules/chat/chat_controller.dart';
import 'package:messenger/modules/chat/chat_repository.dart';
import 'package:messenger/modules/chat/chat_view.dart';

class ChatPage extends StatelessWidget {
  const ChatPage({super.key,required this.currentUserEmail, required this.username});

  final String currentUserEmail;
  final String username;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ChatController>(
      init: ChatController(repository: ChatRepository()),
      initState: (_) => Get.find<ChatController>().onInit(),
      builder: (_) => Scaffold(
        body: ChatView( currentUserEmail: currentUserEmail,),
      ),
    );
  }
}