import 'dart:async';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/instance_manager.dart';
import 'package:intl/intl.dart';
import 'package:messenger/models/user_model.dart';
import 'package:messenger/modules/chat/chat_controller.dart';
import 'package:messenger/modules/chat/chat_details_screen.dart';

class ChatView extends StatefulWidget {
  const ChatView({super.key, required this.currentUserEmail});

  final String currentUserEmail;

  @override
  State<ChatView> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatView> {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  late StreamController<void> _refreshController;

  @override
  void initState() {
    super.initState();
    _refreshController = StreamController<void>();
  }

  @override
  void dispose() {
    _refreshController.close();
    super.dispose();
  }



  void _refreshChatData() {
    _refreshController.add(null);
  }

  @override
  Widget build(BuildContext context) {
    ChatController chatController = Get.find();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Chats'),
        backgroundColor: Colors.blue[800],
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            onPressed: () => chatController.logout(context),
          ),
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _firestore.collection('users').snapshots(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }

          if (snapshot.hasError) {
            return const Center(child: Text('Something went wrong'));
          }

          if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            return const Center(child: Text('No users found'));
          }

          List<UserModel> users = snapshot.data!.docs
              .map((doc) => UserModel.fromMap(doc.data() as Map<String, dynamic>))
              .where((user) => user.email != widget.currentUserEmail)
              .toList();

          if (users.isEmpty) {
            return const Center(child: Text('No other users found'));
          }

          return RefreshIndicator(
            onRefresh: () async {
              _refreshChatData();
            },
            child: ListView.builder(
              itemCount: users.length,
              itemBuilder: (context, index) {
                var user = users[index];
                ChatController chatController = Get.find();
                return Dismissible(
                  key: Key(user.email!), // Unique key for each user
                  direction: user.permission == 2
                      ? DismissDirection.startToEnd
                      : DismissDirection.none,
                  confirmDismiss: user.permission == 2
                      ? (direction) async {
                          return await showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: const Text("Confirm"),
                                content: const Text(
                                    "Are you sure you want to delete this user?"),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.of(context).pop(false),
                                    child: const Text("CANCEL"),
                                  ),
                                  TextButton(
                                    onPressed: () async {
                                      await _firestore
                                          .collection('users')
                                          .doc(user.email)
                                          .delete();
                                      Navigator.of(context).pop(true);
                                    },
                                    child: const Text("DELETE"),
                                  ),
                                ],
                              );
                            },
                          );
                        }
                      : null,
                  background: Container(
                    color: Colors.red,
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.only(left: 20.0),
                    child: const Icon(Icons.delete, color: Colors.white),
                  ),
                  child: GestureDetector(
                    onTap: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChatDetailScreen(
                            email: user.email ?? '',
                            profileImage: user.profileUrl ?? '',
                            permission: user.permission ?? 0,
                            username: user.userName ?? '',
                          ),
                        ),
                      );
                      _refreshChatData();
                    },
                    child: Card(
                      child: ListTile(
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(user.profileUrl ?? ''),
                          radius: 24,
                        ),
                        title: Text(user.userName ?? ''),
                        subtitle: StreamBuilder<List<QueryDocumentSnapshot>>(
                          stream: chatController.fetchLastMessage(user.email ?? ''),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return const Text('Loading...');
                            }

                            if (snapshot.hasError) {
                              return const Text('Error');
                            }

                            var messages = snapshot.data ?? [];
                            if (messages.isEmpty) {
                              return const Text('No messages');
                            }

                            var lastMessage = messages.last;
                            var content = lastMessage['content'] ?? 'No content';
                            var sender = lastMessage['sender'] ?? 'Unknown';

                            bool isCurrentUser =
                                sender == _auth.currentUser?.email;

                            String subtitleText =
                                isCurrentUser ? 'You: $content' : content;

                            return Text(
                              subtitleText,
                              maxLines: 1,
                              style: const TextStyle(color: Colors.grey),
                              overflow: TextOverflow.ellipsis,
                            );
                          },
                        ),
                        trailing: Text(
                          DateFormat('HH:mm').format(DateTime.now()),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
