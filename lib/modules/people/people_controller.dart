import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:messenger/modules/people/people_repository.dart';

class PeopleController extends GetxController{
  PeopleController({required this.repository});

  final PeopleRepository repository;
}