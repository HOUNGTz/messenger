import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:get/instance_manager.dart';
import 'package:messenger/modules/people/people_controller.dart';
import 'package:messenger/modules/people/people_repository.dart';
import 'package:messenger/modules/people/people_view.dart';

class PeoplePage extends StatelessWidget {
  const PeoplePage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PeopleController>(
      init: PeopleController(repository: PeopleRepository()),
      initState: (_) => Get.find<PeopleController>().onInit(),
      builder: (_) => const Scaffold(
        body: PeopleView(),
      ),
    );
  }
}