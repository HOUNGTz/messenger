import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PeopleView extends StatefulWidget {
  const PeopleView({super.key});

  @override
  State<PeopleView> createState() => _PeopleViewState();
}

class _PeopleViewState extends State<PeopleView> {
  final Map<String, bool> _friendRequests = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Friends', style: TextStyle(fontWeight: FontWeight.bold)),
        backgroundColor: Colors.blue[800],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('users').snapshots(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }
          if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            return const Center(child: Text('No users found'));
          }

          final users = snapshot.data!.docs;

          return ListView.builder(
            itemCount: users.length,
            itemBuilder: (context, index) {
              final user = users[index];
              final data = user.data() as Map<String, dynamic>;

              final profile = data['profile'] ?? 'https://www.example.com/default_profile.jpg';
              final username = data['username'] ?? 'Unknown';
              final status = data['status'] ?? 'No status';
              final userId = user.id;

              bool isFriendRequested = _friendRequests[userId] ?? false;

              return ListTile(
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(profile),
                ),
                title: Text(username, style: const TextStyle(fontWeight: FontWeight.bold)),
                subtitle: Text(status),
                trailing: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      _friendRequests[userId] = !isFriendRequested;
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: isFriendRequested ? Colors.grey : Colors.blue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                  child: Text(isFriendRequested ? 'Cancel' : 'Add Friend'),
                ),
                contentPadding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
              );
            },
          );
        },
      ),
    );
  }
}
