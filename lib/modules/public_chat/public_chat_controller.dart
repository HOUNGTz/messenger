import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:messenger/modules/public_chat/public_chat_repository.dart';

class PublicChatController extends GetxController{
  PublicChatController({required this.repository});

  PublicChatRepository repository;
}