import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:get/instance_manager.dart';
import 'package:messenger/modules/public_chat/public_chat_controller.dart';
import 'package:messenger/modules/public_chat/public_chat_repository.dart';
import 'package:messenger/modules/public_chat/public_chat_view.dart';

class PublicChatPage extends StatelessWidget {
  const PublicChatPage({super.key, required this.username});

  final String username;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PublicChatController>(
      init: PublicChatController(repository: PublicChatRepository()),
      initState: (_) => Get.find<PublicChatController>().onInit(),
      builder: (_) => Scaffold(
        body: PublicChatView(username: username,),
      ),
    );
  }
}