import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';

class PublicChatView extends StatefulWidget {
  const PublicChatView({super.key, required this.username});

  final String username;

  @override
  _PublicChatViewState createState() => _PublicChatViewState();
}

class _PublicChatViewState extends State<PublicChatView> {
  final TextEditingController _messageController = TextEditingController();
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final ScrollController _scrollController = ScrollController();
  User? currentUser;

  @override
  void initState() {
    super.initState();
    currentUser = _auth.currentUser;
  }

  void _sendMessage() async {
    if (_messageController.text.trim().isEmpty) return;

    if (currentUser == null) return;

    String username = currentUser?.displayName ?? 'Anonymous';
    String profileImage = currentUser?.photoURL ?? 'https://www.example.com/default_profile.jpg';
    await _firestore.collection('public_messages').add({
      'sender': currentUser!.email,
      'username': username,
      'profileImage': profileImage,
      'timestamp': DateTime.now(),
      'content': _messageController.text.trim(),
    });

    _messageController.clear();

    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeOut,
    );
  }

  Stream<QuerySnapshot> _fetchMessages() {
    return _firestore
        .collection('public_messages')
        .orderBy('timestamp', descending: false)
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Global Chat'),
      ),
      body: Column(
        children: [
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
              stream: _fetchMessages(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: CircularProgressIndicator());
                }

                if (snapshot.hasError) {
                  return const Center(child: Text('Error fetching messages.'));
                }

                var allMessages = snapshot.data?.docs ?? [];

                if (allMessages.isEmpty) {
                  return const Center(
                      child: Text('No messages yet. Start the conversation!'));
                }

                WidgetsBinding.instance.addPostFrameCallback((_) {
                  if (_scrollController.hasClients) {
                    _scrollController.animateTo(
                      _scrollController.position.maxScrollExtent,
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.easeOut,
                    );
                  }
                });

                return ListView.builder(
                  controller: _scrollController,
                  itemCount: allMessages.length,
                  itemBuilder: (context, index) {
                    var message = allMessages[index];
                    var isSender =
                        message['sender'] == currentUser?.email;
                    var messageTime =
                        (message['timestamp'] as Timestamp).toDate();
                    var timeFormat = DateFormat('HH:mm').format(messageTime);

                    return GestureDetector(
                      onLongPress: () => _showMessageOptions(context, message),
                      child: Align(
                        alignment: isSender
                            ? Alignment.centerRight
                            : Alignment.centerLeft,
                        child: Container(
                          margin: const EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 8.0),
                          padding: const EdgeInsets.all(12.0),
                          decoration: BoxDecoration(
                            color: isSender ? Colors.blue : Colors.grey[200],
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                message['username'] ?? 'Unknown',
                                style: TextStyle(
                                  color: isSender ? Colors.white : Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14.0,
                                ),
                              ),
                              const SizedBox(height: 4.0),
                              Text(
                                message['content'] ?? 'No content',
                                style: TextStyle(
                                  color: isSender ? Colors.white : Colors.black,
                                  fontSize: 16.0,
                                ),
                              ),
                              const SizedBox(height: 4.0),
                              Text(
                                timeFormat,
                                style: TextStyle(
                                  color:
                                      isSender ? Colors.white70 : Colors.black54,
                                  fontSize: 12.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ),
          _buildFooter(),
        ],
      ),
    );
  }

  void _showMessageOptions(BuildContext context, QueryDocumentSnapshot message) {
    var isSender = message['sender'] == currentUser?.email;

    if (!isSender) return;

    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              leading: const Icon(Icons.edit),
              title: const Text('Edit'),
              onTap: () {
                Navigator.pop(context);
                _showEditDialog(context, message);
              },
            ),
            ListTile(
              leading: const Icon(Icons.delete),
              title: const Text('Delete'),
              onTap: () {
                Navigator.pop(context);
                _deleteMessage(message.id);
              },
            ),
          ],
        );
      },
    );
  }

  void _showEditDialog(BuildContext context, QueryDocumentSnapshot message) {
    TextEditingController _editController =
        TextEditingController(text: message['content']);

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Edit Message'),
          content: TextField(
            controller: _editController,
            decoration: const InputDecoration(hintText: 'Edit your message'),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                _editMessage(message.id, _editController.text.trim());
                Navigator.pop(context);
              },
              child: const Text('Save'),
            ),
          ],
        );
      },
    );
  }

  void _editMessage(String messageId, String newContent) async {
    await _firestore.collection('public_messages').doc(messageId).update({
      'content': newContent,
    });
  }

  void _deleteMessage(String messageId) async {
    await _firestore.collection('public_messages').doc(messageId).delete();
  }

  Widget _buildFooter() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: _messageController,
              decoration: InputDecoration(
                hintText: 'Type a message...',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(24.0),
                ),
                contentPadding: const EdgeInsets.symmetric(horizontal: 16.0),
              ),
              textInputAction: TextInputAction.send,
              keyboardType: TextInputType.text,
              onSubmitted: (_) {
                _sendMessage();
              },
            ),
          ),
          const SizedBox(width: 8.0),
          IconButton(
            icon: const Icon(Icons.send),
            onPressed: _sendMessage,
          ),
        ],
      ),
    );
  }
}
