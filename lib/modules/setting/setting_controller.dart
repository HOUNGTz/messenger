import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:messenger/modules/setting/setting_repository.dart';

class SettingController extends GetxController{
  SettingController({required this.repository});
  final SettingRepository repository;
}