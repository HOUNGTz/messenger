import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:get/instance_manager.dart';
import 'package:messenger/modules/setting/setting_controller.dart';
import 'package:messenger/modules/setting/setting_repository.dart';
import 'package:messenger/modules/setting/setting_view.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SettingController>(
      init: SettingController(repository: SettingRepository()),
      initState: (_) => Get.find<SettingController>().onInit(),
      builder: (_) => const Scaffold(
        body: SettingView(),
      ),
    );
  }
}